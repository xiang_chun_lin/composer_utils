<?php

namespace Utils\Validate;

use PHPUnit\Framework\TestCase;

/**
 * 身份证验证器单元测试
 *
 * @package Utils\Validate
 * @author xiangchunlin
 * @date 2023/7/22
 */
class IdNumberValidateTest extends TestCase
{

    /**
     * 测试身份证格式验证是否正确
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testValidateType()
    {
        // 测试15位正确的身份证号
        $this->assertTrue(IdNumberValidate::validateType("320483770101001"));

        // 测试15位错误的身份证号
        $this->assertFalse(IdNumberValidate::validateType("32345678902222"));

        // 测试18位正确的身份证号
        $this->assertTrue(IdNumberValidate::validateType("12010519920215731X"));

        // 测试18位错误的身份证号（校验位不正确）
        $this->assertFalse(IdNumberValidate::validateType("51010319870101641X"));

        // 测试位数不正确的身份证号（小于 18 位）
        $this->assertFalse(IdNumberValidate::validateType("12345678901234567"));

        // 测试位数不正确的身份证号（大于 18 位）
        $this->assertFalse(IdNumberValidate::validateType("1234567890123456789"));
    }

    /**
     * 测试15位身份证号转18位
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testFifteen2Eighteen()
    {
        // 测试15位转18位
        $this->assertEquals("320483197701010017", IdNumberValidate::fifteen2Eighteen("320483770101001"));
    }

    /**
     * 测试校验码生成
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testBuildCode()
    {
        $this->assertEquals("7", IdNumberValidate::buildCode("33042119930728900"));
        $this->assertEquals("1", IdNumberValidate::buildCode("41132719960728180"));
        $this->assertEquals("9", IdNumberValidate::buildCode("41132719960728400"));
    }

    /**
     * 测试验证校验码
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testValidateCode()
    {
        // 测试正确身份证号
        $this->assertTrue(IdNumberValidate::validateCode("530623199607286503"));

        // 测试校验码错误的身份证号
        $this->assertFalse(IdNumberValidate::validateCode("530623199607282802"));
    }

    /**
     * 测试验证省份信息
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testValidateProvince()
    {
        $this->assertTrue(IdNumberValidate::validateProvince("820102199607288509"));
        $this->assertTrue(IdNumberValidate::validateProvince("810101199607280103"));
        $this->assertTrue(IdNumberValidate::validateProvince("320206199607286004"));
        $this->assertTrue(IdNumberValidate::validateProvince("510107199607285200"));
        $this->assertTrue(IdNumberValidate::validateProvince("500102199607282401"));
    }


    /**
     * 测试身份证年龄
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testGetAge()
    {
        // 测试异常身份证
        $this->assertEquals(-1, IdNumberValidate::getSex("110101199502212101"));

        $this->assertEquals(31, IdNumberValidate::getAge("51010719910728050X"));
        $this->assertEquals(29, IdNumberValidate::getAge("510107199307285401"));
        $this->assertEquals(26, IdNumberValidate::getAge("110101199707221914"));
        $this->assertEquals(20, IdNumberValidate::getAge("510107200207288408"));
    }

    /**
     * 测试身份证性别
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testGetSex()
    {
        // 测试异常身份证
        $this->assertEquals(-1, IdNumberValidate::getSex("110101199502212502"));

        $this->assertEquals(1, IdNumberValidate::getSex("110101200007282316"));
        $this->assertEquals(0, IdNumberValidate::getSex("110101200007286800"));
        $this->assertEquals(1, IdNumberValidate::getSex("110101200207282919"));
        $this->assertEquals(0, IdNumberValidate::getSex("110101200807282808"));
    }

    /**
     * 测试身份证生日
     * @return void
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public function testGetBirthday()
    {
        // 测试异常身份证
        $this->assertEquals(-1, IdNumberValidate::getBirthday("11010120080728930x"));

        $this->assertEquals(20080728, IdNumberValidate::getBirthday("110101200807289305"));
        $this->assertEquals(20010919, IdNumberValidate::getBirthday("110101200109194202"));
        $this->assertEquals(20051102, IdNumberValidate::getBirthday("110101200511021502"));
        $this->assertEquals(19950221, IdNumberValidate::getBirthday("110101199502213208"));
    }
}
