<?php

namespace Utils\Validate;

/**
 * Class IdNumberValidate
 *
 * 身份证验证器
 *
 * 这个类主要用来验证身份证号码的正确性,以及提取身份证号上的信息
 *
 * 身份证号规则说明：
 * 15位: 省份(2位) + 地级市(2位) + 县级市(2位) + 出生年(2位) + 出生月(2位) + 出生日(2位) + 顺序号(3位)
 * 18位: 省份(2位) + 地级市(2位) + 县级市(2位) + 出生年(4位) + 出生月(2位) + 出生日(2位) + 顺序号(3位) + 校验位(1位)
 * 顺序号如果是偶数，则说明是女生，顺序号是奇数，则说明是男生。
 *
 * @package Xiangchunlin\Validate
 * @author xiangchunlin
 * @date 2023/7/22
 */
class IdNumberValidate
{
    /**
     * 验证身份证格式是否正确
     * @param string $id_number 身份证号码
     * @return bool 验证成功返回true, 失败返回false
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function validateType(string $id_number): bool
    {
        // 去除空格和换行符
        $id_number = trim($id_number);

        // 检查身份证号格式
        if (! preg_match('#(^\d{15}$)|(^\d{17}(\d|X)$)#', $id_number)) return false;

        // 将15位身份证号转为18位
        $id_number = self::fifteen2Eighteen($id_number);

        // 检查省份信息
        if (! self::validateProvince($id_number)) return false;

        // 检查校验码
        return self::validateCode($id_number);
    }

    /**
     * 15位身份证号转为18位身份证号
     * @param string $id_number 15位身份证号
     * @return string 18位身份证号
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function fifteen2Eighteen(string $id_number): string
    {
        // 判断身份证位数是否为15位
        if (strlen($id_number) != 15) {
            return $id_number;
        }
        // 一般情况下年份补充都是加上 19 就可以了
        $code = '19';
        $id_number_base = substr($id_number, 0, 6) . $code . substr($id_number, 6, 9);
        // 补充校验位信息返回
        return $id_number_base . self::buildCode($id_number_base);
    }

    /**
     * 生成身份证号的校验码信息
     * @param string $id_number_base 身份证前17数据,除去校验码信息
     * @return string 校验码
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function buildCode(string $id_number_base): string
    {
        // 计算身份证长度
        $id_number_len = strlen($id_number_base);
        // 检查长度是否符合要求
        if ($id_number_len != 17) {
            return false;
        }
        $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        $verify_numbers = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
        $sum = 0;
        for ($i = 0; $i < $id_number_len; $i++) {
            $sum += substr($id_number_base, $i, 1) * $factor[$i];
        }
        $index = $sum % 11;
        // 返回校验码
        return $verify_numbers[$index];
    }

    /**
     * 验证校验码是否正确
     * @param string $id_number 身份证号
     * @return bool 校验码正确返回true, 错误返回false
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function validateCode(string $id_number): bool
    {
        $id_number_base = substr($id_number, 0, 17);
        // 重新生成校验码
        $code = self::buildCode($id_number_base);
        // 比较是否相等
        return $id_number == ($id_number_base . $code);
    }

    /**
     * 验证省份是否正确
     * @param string $id_number 身份证号
     * @return bool 正确返回true, 错误返回false
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function validateProvince(string $id_number): bool
    {
        $provinces = [
            11 => "北京", 12 => "天津", 13 => "河北",   14 => "山西", 15 => "内蒙古",
            21 => "辽宁", 22 => "吉林", 23 => "黑龙江", 31 => "上海", 32 => "江苏",
            33 => "浙江", 34 => "安徽", 35 => "福建",   36 => "江西", 37 => "山东", 41 => "河南",
            42 => "湖北", 43 => "湖南", 44 => "广东",   45 => "广西", 46 => "海南", 50 => "重庆",
            51 => "四川", 52 => "贵州", 53 => "云南",   54 => "西藏", 61 => "陕西", 62 => "甘肃",
            63 => "青海", 64 => "宁夏", 65 => "新疆",   81 => "香港", 82 => "澳门", 83 => "台湾", 91 => "国外"
        ];
        return isset($provinces[substr($id_number, 0, 2)]);
    }

    /**
     * 获取身份证号码对应的出生日期
     * @param string $id_number 身份证号码
     * @return int 返回出生日期，异常则返回 -1
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function getBirthday(string $id_number): int
    {
        //验证身份证格式
        if (! self::validateType($id_number)) return -1;

        // 提取出生日期
        $year = substr($id_number, 6, 4);
        $month = substr($id_number, 10, 2);
        $day = substr($id_number, 12, 2);

        // 构造出生日期字符串
        return $year . $month . $day;
    }

    /**
     * 根据身份证号计算年龄
     * @param string $id_number 身份证号码
     * @return int 返回当前年龄，异常则返回 -1
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function getAge(string $id_number): int
    {
        //获取生日日期
        $birthday = self::getBirthday($id_number);
        //检查日期
        if ($birthday == -1) return $birthday;

        //当前时间
        $today_datetime = new \DateTime();
        try {
            //生日日期
            $birthday_datetime = new \DateTime($birthday);
        } catch (\Exception $e) {
            // 出现格式化异常
            return -1;
        }

        // 计算时间差值
        $diff = $today_datetime->diff($birthday_datetime);
        // printf(' 你的年龄是 : %d 岁, %d 月, %d 天', $diff->y, $diff->m, $diff->d);

        // 返回年龄
        return $diff->y;
    }

    /**
     * 根据身份证号获取用户性别
     * @return int 男性为1, 女性为0, 异常则返回 -1
     * @author xiangchunlin
     * @date 2023/7/22
     */
    public static function getSex(string $id_number): int
    {
        // 验证身份证格式
        if (! self::validateType($id_number)) return -1;
        //提取倒数第二位数字
        $gender_digit = intval(substr($id_number, -2, 1));
        // 判断奇偶
        return $gender_digit % 2 === 0 ? 0 : 1;
    }
}